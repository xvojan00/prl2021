set terminal pdf
set output "ots_perf_graph_full.pdf"
set datafile separator ','
set key autotitle columnhead
set ylabel "Čas potřebný k seřazení [ms]"
set xlabel "Počet řazených položek"
set key right bottom
f(x)=x/205
g(x)=x/195
plot "ots_perf_avg.csv" using 1:(10**3*$2) with lines, \
		 f(x) title "f(x) = x / 205" with lines, \
		 g(x) title "g(x) = x / 195" with lines
