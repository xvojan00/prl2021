#!/usr/bin/env bash

OUTPUT=ots_perf_new.csv
INPUT="../test.sh"

# Start with 2 values, end with 2^22
START=4
STOP=1048576

for (( i=$START; i<=$STOP; i=i*2 ))
do
    echo "Running with $i"
    line="$i"
    for (( j=0; j<32; j++ ))
    do
        val=$($INPUT $i | tail -n 1 | awk '{ print $2 }')
        line="$line,$val"
    done
    echo $line
    echo $line >> $OUTPUT
done
