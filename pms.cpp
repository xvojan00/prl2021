/**
 * @file ots.cpp
 * @brief Main implementation file of pipeline merge sort
 * @author Kamil Vojanec <xvojan00@stud.fit.vutbr.cz>
*/

#include <mpi.h>
#include <iostream>
#include <queue>
#include <cmath>
#include <fstream>

#ifdef NDEBUG
#define DEBUG(core, msg1, msg2, msg3) do { \
    int rank; \
    MPI_Comm_rank(MPI_COMM_WORLD, &rank); \
    if (rank == core) { \
        std::cout << "CORE " << rank << " " << msg1 << " " << msg2 << " " << msg3 << std::endl; \
    } \
} while(0)
#else
#define DEBUG(a,b,c,d)
#endif

/**
 * @brief Enumeration for all MPI tags used in the program.
 */
enum Tag {
    TAG_Q1 = 1,
    TAG_Q2 = 2,
    TAG_SENTINEL = 3,
};

/**
 * @brief Base class implementing 'send' method for sending one byte
 * using MPI.
 *
 * @tparam T Data type to be sent.
 */
template<typename T>
class Sender {
    public:
        /**
         * @brief Construct a sender object.
         *
         * @param mpi_rank MPI rank of assigned process.
         */
        explicit Sender<T>(const int mpi_rank) : mpi_rank(mpi_rank) {};
        /**
         * @brief Send one byte through MPI.
         *
         * @param tag Tag to be sent along with the data.
         * @param val Value to be sent.
         */
        void send(const int tag, const T val);

    private:
        int mpi_rank;
};

template<typename T>
void Sender<T>::send(const int tag, const T val) {
    MPI_Send(&val, 1, MPI_BYTE, mpi_rank+1, tag, MPI_COMM_WORLD);
}

/**
 * @brief Base class implementing 'receive' method for receiving
 * data through MPI.
 *
 * @tparam T Type to receive.
 */
template<typename T>
class Receiver {
    public:
        /**
         * @brief Construct a receiver object.
         *
         * @param mpi_rank MPI rank of assigned process.
         */
        Receiver<T>(const int mpi_rank):
            mpi_rank(mpi_rank) {};
        /**
         * @brief Receive one byte through MPI and push it to a queue
         * specified by received tag.
         *
         * @param q1 First queue of pipeline merge sort algorithm.
         * @param q2 Second queue of pipeline merge sort algorithm.
         *
         * @return 1 if received valid data, 0 if sentinel tag is received.
         */
        int receive(std::queue<T>& q1, std::queue<T>& q2);
    private:
        int mpi_rank;
};

template<typename T>
int Receiver<T>::receive(std::queue<T>& q1, std::queue<T>& q2) {
    MPI_Status status;
    T val;

    // Receive the value with any tags.
    MPI_Recv(&val, 1, MPI_BYTE, mpi_rank-1, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

    // Decide which queue to push to according to the tag.
    if (status.MPI_TAG == TAG_Q1) {
        q1.push(val);
        return 1;
    } else if (status.MPI_TAG == TAG_Q2) {
        q2.push(val);
        return 1;
    } else {
        // Tag was either invalid or sentinel.
        return 0;
    }
}

/**
 * @brief First process in the pipeline of pipeline merge sort algorithm.
 *
 * @tparam T Data type of the algorithm.
 */
template<typename T>
class InitProcess : public Sender<T> {
    public:
        /**
         * @brief Construct an InitProcess object.
         *
         * @param mpi_rank MPI rank of assigned process.
         */
        explicit InitProcess<T>(const int mpi_rank) : Sender<T>(mpi_rank), mpi_rank(mpi_rank) {};
        /**
         * @brief Fill the input queue with data from file.
         *
         * @param filepath Path to file.
         */
        void set_input_from_file(const std::string &filepath);
        /**
         * @brief Read from input queue, create sorted sequences of length 1
         * and send them to next process' appropriate queue.
         */
        void run();
    private:
        std::queue<T> in_queue;
        int mpi_rank;
};

template<typename T>
void InitProcess<T>::set_input_from_file(const std::string &filepath) {
    std::ifstream file;

    // Open file
    file.open(filepath, std::ios::in | std::ios::binary);
    while(file) {
        // Read file byte by byte and push to input queue.
        auto val = file.get();
        if (!file) break;
        std::cout << val << " ";
        in_queue.push(val);
    }
    std::cout << std::endl;
    file.close();
}

template<typename T>
void InitProcess<T>::run() {
    int cnt = 0;
    // Read from input queue
    while (!in_queue.empty()) {
        auto v = in_queue.front();
        in_queue.pop();
        // Alternate between q1 and q2 of the next process.
        this->send((cnt & 1) + 1, v);
        cnt++;
    }
    // Finally, send a sentinel to signal the end.
    T z = 0;
    this->send(TAG_SENTINEL, z);
}

/**
 * @brief Class representing a merging process in pipeline merge sort.
 *
 * @tparam T Data type to be merged.
 */
template<typename T>
class InnerProcess : public Receiver<T>, public Sender<T> {
    public:
        /**
         * @brief Construct an InnerProcess object.
         *
         * @param mpi_rank MPI rank of assigned process.
         */
        explicit InnerProcess<T>(int mpi_rank):
            Receiver<T>(mpi_rank), Sender<T>(mpi_rank),
            merged_sequence_len(std::pow(2, mpi_rank)),
            mpi_rank(mpi_rank),
            q1_s(0),
            q2_s(0),
            queue_tag_out(TAG_Q1),
            received(true),
            started(false) {};
        /**
         * @brief Read from input queues, generate sorted subsequences and send them
         * to next process.
         */
        void run();

    private:
        std::queue<T> q1;
        std::queue<T> q2;

        unsigned int merged_sequence_len;
        int mpi_rank;

        unsigned int q1_s;
        unsigned int q2_s;
        int queue_tag_out;
        bool received;
        bool started;

        /**
         * @brief End one merging sequence, resetting temporary counters
         * and changing the output tag to send next sequence to the other
         * queue.
         */
        void conclude_merge();
        /**
         * @brief Send value from q1.
         */
        void process_q1();
        /**
         * @brief Send value from q2.
         */
        void process_q2();
};

template<typename T>
void InnerProcess<T>::conclude_merge() {
    // Reset counters
    q1_s = 0;
    q2_s = 0;
    // Alternate queues.
    queue_tag_out = queue_tag_out == TAG_Q1 ? TAG_Q2 : TAG_Q1;
}

template<typename T>
void InnerProcess<T>::process_q1() {
    // Read from queue.
    auto v = q1.front();
    q1.pop();
    q1_s++;
    // Send value.
    this->send(queue_tag_out, v);
}

template<typename T>
void InnerProcess<T>::process_q2() {
    // Read from queue.
    auto v = q2.front();
    q2.pop();
    q2_s++;
    // Send value.
    this->send(queue_tag_out, v);
}

template<typename T>
void InnerProcess<T>::run() {
    while (received || !q1.empty() || !q2.empty()) {
        if (q1_s + q2_s >= merged_sequence_len) {
            // We have reached the maximum length of sorted sequence.
            conclude_merge();
        }

        if (!q1.empty() && !q2.empty()) {
            // We can start merging now.
            started = 1;
            auto v1 = q1.front();
            auto v2 = q2.front();

            if (q1_s >= merged_sequence_len/2) {
                // The maximum number of values that can be read from q1
                // has been reached. The rest of the values will be read
                // from q2.
                process_q2();
            }
            else if (q2_s >= merged_sequence_len/2) {
                // The maximum number of values that can be read from q2
                // has been reached. The rest of the values will be read
                // from q1.
                process_q1();
            }
            else {
                // Decide based on the values
                if (v1 < v2) {
                    process_q1();
                }
                else {
                    process_q2();
                }
            }
        }
        else if (!q1.empty() && q2.empty() && started && !received) {
            // We no longer have both queues available. Send the rest from
            // q1 and end.
            process_q1();
        }
        else if (!q2.empty() && q1.empty() && started && !received) {
            // We only have q2 available. Send the rest from q2 and end.
            process_q2();
        }

        if (received) {
            // Receive only if sentinel has not been received.
            received = this->receive(q1, q2);
        }
    }
    // This process finished, signal sentinel to the next process in line.
    this->send(received, TAG_SENTINEL);
}

/**
 * @brief Class representing the final process in pipeline merge sort.
 * This class is responsible for performing the final merge and printing
 * values to standard output.
 *
 * @tparam T Data type to handle.
 */
template<typename T>
class FinalProcess : public Receiver<T> {
    public:
        /**
         * @brief Construct a FinalProcess object.
         *
         * @param mpi_rank MPI rank of assigned process.
         */
        explicit FinalProcess<T>(int mpi_rank):
            Receiver<T>(mpi_rank),
            mpi_rank(mpi_rank),
            received(true),
            started(false) {};
        /**
         * @brief Perform the final merge into output queue.
         */
        void run();
        /**
         * @brief Print the output queue.
         */
        void print();
    private:
        std::queue<T> q1;
        std::queue<T> q2;

        std::queue<T> out_queue;

        int mpi_rank;
        bool received;
        bool started;
};

template<typename T>
void FinalProcess<T>::run() {
    while (received || !q1.empty() || !q2.empty()) {
        if (!q1.empty() && !q2.empty()) {
            // We can start merging into the output queue.
            started = true;
            auto v1 = q1.front();
            auto v2 = q2.front();

            // Only decide based on values.
            if (v1 < v2) {
                q1.pop();
                out_queue.push(v1);
            }
            else {
                q2.pop();
                out_queue.push(v2);
            }
        }
        else if (!q1.empty() && q2.empty() && started && !received) {
            // We can only use q1.
            auto v = q1.front();
            q1.pop();
            out_queue.push(v);
        }
        else if (!q2.empty() && q1.empty() && started && !received) {
            // We can only use q2.
            auto v = q2.front();
            q2.pop();
            out_queue.push(v);
        }

        if (received) {
            // Receive only if sentinel has not been received.
            received = this->receive(q1, q2);
        }
    }
    // We do not send the sentinel as this will be the final process.
}

template<typename T>
void FinalProcess<T>::print() {
    while (!out_queue.empty()) {
        // Loop through the queue and print values.
        auto v = out_queue.front();
        out_queue.pop();
        std::cout << static_cast<int>(v) << std::endl;
    }
}

int main(int argc, char **argv) {
    int mpi_rank;
    int mpi_comm_size;

    // Initialize MPI
    MPI_Init(&argc, &argv);

    // Get MPI values
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_comm_size);

    // Set Initial process of the pipeline.
    if (mpi_rank == 0) {
        auto first = InitProcess<uint8_t>(mpi_rank);
        first.set_input_from_file("numbers");
        first.run();
    }
    // Set final process of the pipeline.
    if (mpi_rank == mpi_comm_size-1) {
        auto fin = FinalProcess<uint8_t>(mpi_rank);
        fin.run();
        //stop = std::chrono::system_clock::now();
        fin.print();
        fflush(stdout);
    }

    // Set all inner processes.
    if (mpi_rank > 0 && mpi_rank < (mpi_comm_size-1)) {
        auto middle = InnerProcess<uint8_t>(mpi_rank);
        middle.run();
    }

    // End MPI.
    MPI_Finalize();
		return 0;
}
