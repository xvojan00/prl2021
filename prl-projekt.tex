\documentclass[a4paper,]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{float}
\usepackage{amsmath}
\usepackage[left=2cm, text={17cm, 24cm}, top=3cm]{geometry}
\usepackage[czech]{babel}
\usepackage{hyperref}
\usepackage{graphicx}

\title{Paralelní a distribuované algoritmy\\ \large{Pipeline merge sort}}
\author{Kamil Vojanec\\
        xvojan00@stud.fit.vutbr.cz}
\date{\today}

\begin{document}
\maketitle
\section{Rozbor algoritmu}
Algoritmus pracuje na principu slučování (\emph{merging}) posloupností hodnot jednotlivými procesory
ve zřetězené lince. Každý procesor spravuje dvojici front, v nichž jsou uloženy již seřazené posloupnosti,
které budou daným procesorem sloučeny. Předpokládejme počet procesorů $P$ a počet vstupních hodnot $n$.
Jednotlivé procesory pracují následovně:

\begin{description}
        \item[První proces (index 0)]
                Načte data z vstupního souboru, a postupně vytváří seřazené posloupnosti délky 1, tedy
                střídá fronty, do nichž vkládá zpracované hodnoty.

        \item[Následující procesy (index $\geq 1$)]
                Načítají hodnoty ze svých vstupních front a vytvářejí posloupnosti délky $2^i$
                (tedy druhý procesor délky 2, třetí délky 4 \dots). Slučování posloupností může
                začít tehdy, je-li v obou vstupních frontách alespoň jedna hodnota. Protože ale
                předchozí procesor střídá výstupní frontu vždy až po dokončení celé své seřazené
                posloupnosti. V praxi tedy každý procesor čeká $2^{i-1}+1$ iterací po začátku
                zpracování předchozího procesoru, než může sám začít zpracovávat hodnoty ve svých
                frontách.

        \item[Poslední procesor (index $P-1$)]
                Ze dvou vstupních front o maximální délce $\frac{n}{2}$ vytváří jednu výstupní
                posloupnost o délce $n$. Tato posloupnost je již seřazena. Pro začátek
                slučování platí stejné podmínky jako pro předchozí procesory.
\end{description}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\textwidth]{prl-diagram1.pdf}
    \caption{Blokové schéma procesů a front mezi nimi na příkladu řazení 8 hodnot.}
    \label{fig:block_schematic}
\end{figure}

\section{Časová složitost a cena algoritmu}

Jak je uvedeno výše, každý procesor čeká na začátek zpracování $2^{n-1}+1$ iterací po začátku
zpracování předchozího procesoru. Počet iterací, které procesor čeká od začátku výpočtu je tedy:

\begin{equation*}
        i \times (2^{i-1}+1) = 2^i+i
\end{equation*}

Kde $i$ udává index daného procesoru. Pro určení celkové časové složitosti je však třeba určit
počet iterací, které čeká poslední procesor na vstupní data a pak přičíst počet iterací, které
poslední procesor stráví samotným zpracováním. Uvažujme počet vstupních hodnot $n$, pak počet
iterací pro zpracování hodnot posledním procesorem je:

\begin{equation*}
        2^{\log_2{n}} + \log_2{n} + (n - 1) = 2n + \log_2{n} -1
\end{equation*}

Z tohoto vzorce je patrné, že časová složitost nejhoršího případu je $O(n)$. Cena algoritmu je
dána jakou součin časové složitosti nejhoršího případu a počtu použitých procesorů:

\begin{equation*}
        c(n) = O(n) \times (\log_2{n} + 1) = O(n\log_2{n})
\end{equation*}

Výsledná cena algoritmu odpovídá časové složitosti nejhoršího případu pro optimální sekvenční
algoritmus, tedy cena je optimální.

\section{Implementace}

Program je implementován v jazyce \emph{C++} s využitím knihovny \emph{OpenMPI}. Pro komunikaci
mezi procesy jsou implementovány bázové třídy \emph{Receiver} a \emph{Sender}, které přijímají,
respektive zasílají zprávy mezi procesy. Rozlišení, do které fronty se hodnoty přiřadí je provedeno
na straně odesilatele pomocí nastavení \emph{MPI tagu}.

Práce na jednotlivých procesorech je zapouzdřena do tříd:

\begin{description}
        \item[InitProcess]
                Načítá data ze vstupního souboru a rozesílá je střídavě na fronty $q_1$,
                $q_2$ následujícího procesoru. K odesílání dat využívá dědičnosti třídy
                \emph{Sender}, která obsahuje definici metody \emph{send} pro zasílání zpráv
                pomocí knihovny \emph{OpenMPI}.
        \item[InnerProcess]
                Přijímá data zaslaná předchozím procesem a zapisuje je na konec fronty.
                K účelu přijímání dat je využito dědičnosti ze třídy \emph{Receiver}.
                Hlavní činnost procesu je realizována metodou \emph{run}, která čte z
                obou front a zajišťuje sloučení posloupností do výstupní posloupnosti,
                která je poslána dalšímu procesu. K odesílání zpráv dalšímu procesu je
                opět využito dědičnosti z třídy \emph{Sender}.
        \item[FinalProcess]
                Na vstup přijímá data ze dvou front, slučuje je do jedné výstupní
                posloupnosti a po naplnění výstupu vytiskne posloupnost na standardní
                výstup. Přijímání dat je realizováno děděním třídy \emph{Receiver}.
\end{description}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{prl-diagram2.pdf}
    \caption{Diagram tříd užitý k implementaci pipeline merge sort algoritmu.}
    \label{fig:class_diagram}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\textwidth]{prl-sequence.pdf}
    \caption{Sekvenční diagram zobrazující komunikaci mezi procesy pomocí \emph{OpenMPI}.}
    \label{fig:sequence_diagram}
\end{figure}

\section{Závěr}

Algoritmus \emph{pipeline merge sort} je sice svými parametry optimálním algoritmem. Nicméně použití
meziprocesorové komunikace pomocí zasílání zpráv s sebou nese značnou režii a praktická časová
složitost by zřejmě byla horší. Navíc, algoritmus pro svoji činnost potřebuje $log_2 n + 1$ procesorů,
což značí, že je použitelný jen pro relativně krátké posloupnosti. Celkově se ale podařilo algoritmus
implementovat pro obecný počet procesorů, pokud by byly k dispozici.




\end{document}
