#!/usr/bin/env bash
# test.sh
# Author: Kamil Vojanec <xvojan00@stud.fit.vutbr.cz>
# Date: 1.4.2021
# Description: Runner script for pipeline merge sort implementation.

BIN=pms
NUMBERS_FILE=numbers
SRC=pms.cpp
CXXFLAGS="-Wall -std=c++17 -pedantic"

log() {
    base=$1
    val=$2

    tmp=1

    while [ "$val" -gt "$base" ]; do
        val=$(( val / base ))
        tmp=$(( tmp + 1 ))
    done

    echo $tmp
}

np() {
    echo $(( $(log 2 "$1") + 1 ))
}

# Enter number count
if [ $# -lt 1 ];then
    NUMBERS=16;
else
    NUMBERS=$1;
fi;

# Compile
mpic++ $CXXFLAGS --prefix /usr/local/share/OpenMPI -o $BIN $SRC


# Generate random
dd if=/dev/random bs=1 count="$NUMBERS" of="$NUMBERS_FILE" &>/dev/null

# Run
mpirun --prefix /usr/local/share/OpenMPI --oversubscribe -np "$(np "$NUMBERS")" $BIN

# Clear
rm -f $BIN "$NUMBERS_FILE"
